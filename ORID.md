### O:
- Today, we have a PPT presentation, and our group demonstrated the strategy mode. In the afternoon, we learned to reconstruct the code and find out the bad taste in the code.
### R:
- I have a certain understanding of the strategy pattern, command pattern, and observer pattern in the design pattern.
### I:
- How to use design patterns for development at work is a thing worth thinking about. As well as refactoring and optimizing the historical code so that it can work longer and stay clean.
### D:
- Improve the skills by using design patterns and refactoring code in a timely manner.