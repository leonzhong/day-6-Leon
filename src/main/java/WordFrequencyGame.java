import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String REGEX = "\\s+";
    public static final int SINGLE = 1;
    public static final String LINE_BREAK = "\n";

    public String getResult(String inputStr) {
        try {
            String[] splitWords = getSplitWords(inputStr);
            List<Input> inputList = transformToInputs(splitWords);
            inputList = countFrequency(inputList);
            sortDescending(inputList);
            return formatReport(inputList);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private static String formatReport(List<Input> inputList) {

        StringJoiner joiner = new StringJoiner(LINE_BREAK);
        inputList.forEach(input -> joiner.add(String.format("%s %d", input.getValue(), input.getWordCount())));
        return joiner.toString();
    }

    private static void sortDescending(List<Input> inputList) {
        inputList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
    }


    private List<Input> countFrequency(List<Input> inputList) {
        Map<String, List<Input>> map = getListMap(inputList);
        List<Input> list = new ArrayList<>();
        map.forEach((key, value) -> list.add(new Input(key, value.size())));
        return list;
    }


    private static List<Input> transformToInputs(String[] splitWords) {
        return Arrays.stream(splitWords).map(splitWord -> new Input(splitWord, SINGLE)).collect(Collectors.toList());
    }

    private static String[] getSplitWords(String inputStr) {
        return inputStr.split(REGEX);
    }


    private Map<String, List<Input>> getListMap(List<Input> inputList) {
        Map<String, List<Input>> map = new HashMap<>();
        inputList.forEach(input ->
                map.computeIfAbsent(input.getValue(), (arr) -> new ArrayList<>()).add(input)
        );
        return map;
    }


}
